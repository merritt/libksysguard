# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2022-09-12 11:20+0200\n"
"Last-Translator: Temuri Doghonadze <temuri.doghonadze@gmail.com>\n"
"Language-Team: Georgian <kde-i18n-doc@kde.org>\n"
"Language: ka\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.1.1\n"

#: contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "გარეგნობა"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Sensors Legend"
msgstr "სენსორების ახსნის ჩვენება"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "მიწყობილი გრაფიკები"

#: contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "რბილი ხაზებ"

#: contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "ბადის ხაზების ჩვენება"

#: contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Y ღერძის ჭდეების ჩვენება"

#: contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "გაუმჭვირვალეობით შევსება:"

#: contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "მონაცემის დიაპაზონები"

#: contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Y მონაცემების ავტომატური დიაპაზონი"

#: contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "საიდან (Y):"

#: contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "სადამდე(Y):"

#: contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "დასატოვებელი ისტორიის რაოდენობა:"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 წამი"
msgstr[1] "%1 წამი"
