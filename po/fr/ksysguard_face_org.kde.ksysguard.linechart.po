# Xavier Besnard <xavier.besnard@neuf.fr>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2021-01-07 23:05+0100\n"
"Last-Translator: Xavier Besnard <xavier.besnard@neuf.fr>\n"
"Language-Team: French <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "Apparence"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Sensors Legend"
msgstr "Afficher la légende pour les senseurs"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "Barres empilées"

#: contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "Lignes avec lissage"

#: contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "Afficher le quadrillage"

#: contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Afficher les libellés de l'axe des abscisses"

#: contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "Saisir l'opacité :"

#: contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "Plages de données"

#: contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Plage automatique de données en « Y »"

#: contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "De (Y) :"

#: contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "A (Y) :"

#: contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "Quantité à conserver de l'historique :"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 seconde"
msgstr[1] "%1 secondes"

#~ msgid "Automatic X Data Range"
#~ msgstr "Plage automatique de données en « X »"

#~ msgid "From (X):"
#~ msgstr "De (X) :"

#~ msgid "To (X):"
#~ msgstr "A (X) :"
