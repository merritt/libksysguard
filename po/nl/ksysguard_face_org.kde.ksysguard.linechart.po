# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the libksysguard package.
#
# Freek de Kruijf <freekdekruijf@kde.nl>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: libksysguard\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-08-07 00:17+0000\n"
"PO-Revision-Date: 2021-01-06 13:26+0100\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#: contents/ui/Config.qml:42
#, kde-format
msgid "Appearance"
msgstr "Uiterlijk"

#: contents/ui/Config.qml:47
#, kde-format
msgid "Show Sensors Legend"
msgstr "Legenda van sensors tonen"

#: contents/ui/Config.qml:51
#, kde-format
msgid "Stacked Charts"
msgstr "Gestapelde grafieken"

#: contents/ui/Config.qml:55
#, kde-format
msgid "Smooth Lines"
msgstr "Gladde lijnen"

#: contents/ui/Config.qml:59
#, kde-format
msgid "Show Grid Lines"
msgstr "Toon hulplijnen"

#: contents/ui/Config.qml:63
#, kde-format
msgid "Show Y Axis Labels"
msgstr "Y-aslabels tonen"

#: contents/ui/Config.qml:67
#, kde-format
msgid "Fill Opacity:"
msgstr "Dekking van vulling:"

#: contents/ui/Config.qml:73
#, kde-format
msgid "Data Ranges"
msgstr "Gegevensreeksen"

#: contents/ui/Config.qml:78
#, kde-format
msgid "Automatic Y Data Range"
msgstr "Automatische Y gegevensreeks"

#: contents/ui/Config.qml:82
#, kde-format
msgid "From (Y):"
msgstr "Van (Y):"

#: contents/ui/Config.qml:89
#, kde-format
msgid "To (Y):"
msgstr "Tot (Y):"

#: contents/ui/Config.qml:99
#, kde-format
msgid "Amount of History to Keep:"
msgstr "Hoeveelheid te bewaren geschiedenis:"

#: contents/ui/Config.qml:102
#, kde-format
msgctxt "%1 is seconds of history"
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 seconde"
msgstr[1] "%1 seconden"

#~ msgid "Automatic X Data Range"
#~ msgstr "Automatische X gegevensreeks"

#~ msgid "From (X):"
#~ msgstr "Van (X):"

#~ msgid "To (X):"
#~ msgstr "Tot (X):"
