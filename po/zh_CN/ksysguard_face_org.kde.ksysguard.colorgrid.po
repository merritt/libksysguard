msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-13 00:48+0000\n"
"PO-Revision-Date: 2023-11-11 07:19\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf6-trunk/messages/libksysguard/ksysguard_face_org.kde."
"ksysguard.colorgrid.pot\n"
"X-Crowdin-File-ID: 43483\n"

#: contents/ui/Config.qml:25
#, kde-format
msgid "Use sensor color:"
msgstr "使用传感器颜色："

#: contents/ui/Config.qml:30
#, kde-format
msgid "Number of Columns:"
msgstr "列数："

#: contents/ui/Config.qml:37
#, kde-format
msgctxt "@label"
msgid "Automatic"
msgstr "自动"
