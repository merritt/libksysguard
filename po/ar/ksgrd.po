# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Abd Allatif, 2014.
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2014.
# Zayed Al-Saidi <zayed.alsaidi@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-13 02:06+0000\n"
"PO-Revision-Date: 2023-04-13 10:12+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 21.12.3\n"

#: SensorAgent.cpp:90
#, kde-format
msgctxt "%1 is a host name"
msgid ""
"Message from %1:\n"
"%2"
msgstr ""
"رسالة من %1:\n"
"%2"

#: SensorManager.cpp:49
#, kde-format
msgid "Change"
msgstr "التغيير"

#: SensorManager.cpp:50
#, kde-format
msgid "Rate"
msgstr "المعدّل"

#: SensorManager.cpp:52
#, kde-format
msgid "CPU Load"
msgstr "حِمْل المعالج"

#: SensorManager.cpp:53
#, kde-format
msgid "Idling"
msgstr "الخمول"

#: SensorManager.cpp:54
#, kde-format
msgid "Nice Load"
msgstr "الحِمْل اللطيف"

#: SensorManager.cpp:55
#, kde-format
msgid "User Load"
msgstr "حِمْل عمليات المستخدم"

#: SensorManager.cpp:56
#, kde-format
msgctxt "@item sensor description"
msgid "System Load"
msgstr "حِمْل النظام"

#: SensorManager.cpp:57
#, kde-format
msgid "Waiting"
msgstr "الانتظار"

#: SensorManager.cpp:58
#, kde-format
msgid "Interrupt Load"
msgstr "حِمْل المقاطعة"

#: SensorManager.cpp:59
#, kde-format
msgid "Total Load"
msgstr "الحِمْل الإجمالي"

#: SensorManager.cpp:61
#, kde-format
msgid "Memory"
msgstr "الذاكرة"

#: SensorManager.cpp:62
#, kde-format
msgid "Physical Memory"
msgstr "الذاكرة الفيزيائية"

#: SensorManager.cpp:63
#, kde-format
msgid "Total Memory"
msgstr "مجموع الذاكرة"

#: SensorManager.cpp:64
#, kde-format
msgid "Swap Memory"
msgstr "ذاكرة الإبدال"

#: SensorManager.cpp:65
#, kde-format
msgid "Cached Memory"
msgstr "الذاكرة المخبّئة"

# دارئ/صوان/براح: من سيفهم هذه المصطلحات؟
#: SensorManager.cpp:66
#, kde-format
msgid "Buffered Memory"
msgstr "الذاكرة المخزّنة"

#: SensorManager.cpp:67
#, kde-format
msgid "Used Memory"
msgstr "الذاكرة المُستخدمَة"

#: SensorManager.cpp:68
#, kde-format
msgid "Application Memory"
msgstr "ذاكرة التطبيقات"

#: SensorManager.cpp:69
#, kde-format
msgid "Allocated Memory"
msgstr "الذاكرة المحجوزة"

#: SensorManager.cpp:70
#, kde-format
msgid "Free Memory"
msgstr "الذاكرة الحرّة"

#: SensorManager.cpp:71
#, kde-format
msgid "Available Memory"
msgstr "الذاكرة المتوفرة"

#: SensorManager.cpp:72
#, kde-format
msgid "Active Memory"
msgstr "الذاكرة النشطة"

#: SensorManager.cpp:73
#, kde-format
msgid "Inactive Memory"
msgstr "الذاكرة غير النشطة"

#: SensorManager.cpp:74
#, kde-format
msgid "Wired Memory"
msgstr "الذاكرة السلكية"

#: SensorManager.cpp:75
#, kde-format
msgid "Exec Pages"
msgstr "صفحات التنفيذ"

#: SensorManager.cpp:76
#, kde-format
msgid "File Pages"
msgstr "صفحات الملف"

#: SensorManager.cpp:79
#, kde-format
msgid "Processes"
msgstr "العمليات"

#: SensorManager.cpp:80 SensorManager.cpp:257
#, kde-format
msgid "Process Controller"
msgstr "المتحكِّم في العمليات"

#: SensorManager.cpp:81
#, kde-format
msgid "Last Process ID"
msgstr "معّرف آخر عملية"

#: SensorManager.cpp:82
#, kde-format
msgid "Process Spawn Count"
msgstr "عدد العمليات المنشورة"

#: SensorManager.cpp:83
#, kde-format
msgid "Process Count"
msgstr "عدد العمليات"

#: SensorManager.cpp:84
#, kde-format
msgid "Idle Processes Count"
msgstr "عدد العمليات الخاملة"

#: SensorManager.cpp:85
#, kde-format
msgid "Running Processes Count"
msgstr "عدد العمليات التي تعمل"

#: SensorManager.cpp:86
#, kde-format
msgid "Sleeping Processes Count"
msgstr "عدد العمليات النائمة"

#: SensorManager.cpp:87
#, kde-format
msgid "Stopped Processes Count"
msgstr "عدد العمليات المتوقّفة"

#: SensorManager.cpp:88
#, kde-format
msgid "Zombie Processes Count"
msgstr "عدد عمليات الزومبي"

#: SensorManager.cpp:89
#, kde-format
msgid "Waiting Processes Count"
msgstr "عدد العمليات المنتظرة"

#: SensorManager.cpp:90
#, kde-format
msgid "Locked Processes Count"
msgstr "عدد العمليات المُقفَلة"

#: SensorManager.cpp:92
#, kde-format
msgid "Disk Throughput"
msgstr "إنتاجيّة القرص"

#: SensorManager.cpp:93
#, kde-format
msgctxt "CPU Load"
msgid "Load"
msgstr "الحِمْل"

#: SensorManager.cpp:94
#, kde-format
msgid "Total Accesses"
msgstr "إجمالي الوصولات"

#: SensorManager.cpp:95
#, kde-format
msgid "Read Accesses"
msgstr "وصولات القراءة"

#: SensorManager.cpp:96
#, kde-format
msgid "Write Accesses"
msgstr "وصولات الكتابة"

#: SensorManager.cpp:97
#, kde-format
msgid "Read Data"
msgstr "البيانات المقروءة"

#: SensorManager.cpp:98
#, kde-format
msgid "Written Data"
msgstr "البيانات المكتوبة"

#: SensorManager.cpp:99
#, kde-format
msgid "Milliseconds spent reading"
msgstr "عدد الميليثوانٍ التي مضت للقراءة"

#: SensorManager.cpp:100
#, kde-format
msgid "Milliseconds spent writing"
msgstr "عدد الميليثوانٍ التي مضت للكتابة"

#: SensorManager.cpp:101
#, kde-format
msgid "I/Os currently in progress"
msgstr "الدَخْل/الخَرْج الذي يعمل الآن"

#: SensorManager.cpp:102
#, kde-format
msgid "Pages In"
msgstr "الصفحات الداخلة"

#: SensorManager.cpp:103
#, kde-format
msgid "Pages Out"
msgstr "الصفحات الخارجة"

#: SensorManager.cpp:104
#, kde-format
msgid "Context Switches"
msgstr "مبدلات السياق"

#: SensorManager.cpp:105
#, kde-format
msgid "Traps"
msgstr "الأفخاخ"

#: SensorManager.cpp:106
#, kde-format
msgid "System Calls"
msgstr "استدعاءات النظام"

#: SensorManager.cpp:107
#, kde-format
msgid "Network"
msgstr "الشبكة"

#: SensorManager.cpp:108
#, kde-format
msgid "Interfaces"
msgstr "الواجهات"

#: SensorManager.cpp:109
#, kde-format
msgid "Receiver"
msgstr "المُستقبِل"

#: SensorManager.cpp:110
#, kde-format
msgid "Transmitter"
msgstr "المُرسِل"

#: SensorManager.cpp:112
#, kde-format
msgid "Data Rate"
msgstr "معدّل البيانات"

#: SensorManager.cpp:113
#, kde-format
msgid "Compressed Packets Rate"
msgstr "معدّل الرزم المضغوطة"

#: SensorManager.cpp:114
#, kde-format
msgid "Dropped Packets Rate"
msgstr "معدّل الرزم المُلقاة"

#: SensorManager.cpp:115
#, kde-format
msgid "Error Rate"
msgstr "معدّل الخطأ"

#: SensorManager.cpp:116
#, kde-format
msgid "FIFO Overruns Rate"
msgstr ""

#: SensorManager.cpp:117
#, kde-format
msgid "Frame Error Rate"
msgstr "معدّل أخطاء الإطارات"

#: SensorManager.cpp:118
#, kde-format
msgid "Multicast Packet Rate"
msgstr ""

#: SensorManager.cpp:119
#, kde-format
msgid "Packet Rate"
msgstr "معدّل الرزم"

#: SensorManager.cpp:120
#, kde-format
msgctxt "@item sensor description ('carrier' is a type of network signal)"
msgid "Carrier Loss Rate"
msgstr ""

#: SensorManager.cpp:121 SensorManager.cpp:132
#, kde-format
msgid "Collisions"
msgstr "الاصطدامات"

#: SensorManager.cpp:123
#, kde-format
msgid "Data"
msgstr "البيانات"

#: SensorManager.cpp:124
#, kde-format
msgid "Compressed Packets"
msgstr "الرزم المضغوطة"

#: SensorManager.cpp:125
#, kde-format
msgid "Dropped Packets"
msgstr "الرزم المُلقاة"

#: SensorManager.cpp:126
#, kde-format
msgid "Errors"
msgstr "الأخطاء"

#: SensorManager.cpp:127
#, kde-format
msgid "FIFO Overruns"
msgstr ""

#: SensorManager.cpp:128
#, kde-format
msgid "Frame Errors"
msgstr "أخطاء الإطارات"

#: SensorManager.cpp:129
#, kde-format
msgid "Multicast Packets"
msgstr ""

#: SensorManager.cpp:130
#, kde-format
msgid "Packets"
msgstr "الرزم"

#: SensorManager.cpp:131
#, kde-format
msgctxt "@item sensor description ('carrier' is a type of network signal)"
msgid "Carrier Losses"
msgstr ""

#: SensorManager.cpp:135
#, kde-format
msgid "Sockets"
msgstr "المقابس"

#: SensorManager.cpp:136
#, kde-format
msgid "Total Number"
msgstr "العدد الإجمالي"

#: SensorManager.cpp:137 SensorManager.cpp:258
#, kde-format
msgid "Table"
msgstr "الجدول"

#: SensorManager.cpp:138
#, kde-format
msgid "Advanced Power Management"
msgstr "إدارة الطاقة المتقدّمة"

#: SensorManager.cpp:139
#, kde-format
msgid "ACPI"
msgstr "ACPI"

#: SensorManager.cpp:140
#, kde-format
msgid "Cooling Device"
msgstr "جهاز التبريد"

#: SensorManager.cpp:141
#, kde-format
msgid "Current State"
msgstr "الحالة الآن"

#: SensorManager.cpp:142 SensorManager.cpp:143
#, kde-format
msgid "Thermal Zone"
msgstr "المنطقة الحرارية"

#: SensorManager.cpp:144 SensorManager.cpp:145
#, kde-format
msgid "Temperature"
msgstr "درجة الحرارة"

#: SensorManager.cpp:146
#, kde-format
msgid "Average CPU Temperature"
msgstr "معدّل حرارة المعالج"

#: SensorManager.cpp:147
#, kde-format
msgid "Fan"
msgstr "المروحة"

#: SensorManager.cpp:148
#, kde-format
msgid "State"
msgstr "الحالة"

#: SensorManager.cpp:149
#, kde-format
msgid "Battery"
msgstr "البطارية"

#: SensorManager.cpp:150
#, kde-format
msgid "Battery Capacity"
msgstr "سعة البطارية"

#: SensorManager.cpp:151
#, kde-format
msgid "Battery Charge"
msgstr "شحن البطارية"

#: SensorManager.cpp:152
#, kde-format
msgid "Battery Usage"
msgstr "استخدام البطارية"

#: SensorManager.cpp:153
#, kde-format
msgid "Battery Voltage"
msgstr "توتّر البطارية"

#: SensorManager.cpp:154
#, kde-format
msgid "Battery Discharge Rate"
msgstr "معدّل تفريغ البطارية"

#: SensorManager.cpp:155
#, kde-format
msgid "Remaining Time"
msgstr "الوقت المتبقي"

#: SensorManager.cpp:156
#, kde-format
msgid "Interrupts"
msgstr "الانقطاعات"

#: SensorManager.cpp:157
#, kde-format
msgid "Load Average (1 min)"
msgstr "معدّل الحِمْل (دقيقة واحدة)"

#: SensorManager.cpp:158
#, kde-format
msgid "Load Average (5 min)"
msgstr "معدّل الحِمْل (5 دقائق)"

#: SensorManager.cpp:159
#, kde-format
msgid "Load Average (15 min)"
msgstr "معدّل الحِمْل (15 دقيقة)"

#: SensorManager.cpp:160
#, kde-format
msgid "Clock Frequency"
msgstr "التردّد الساعيّ"

#: SensorManager.cpp:161
#, kde-format
msgid "Average Clock Frequency"
msgstr "معدّل التردّد الساعيّ"

#: SensorManager.cpp:162
#, kde-format
msgid "Hardware Sensors"
msgstr "حساسات العتاد"

#: SensorManager.cpp:163
#, kde-format
msgid "Partition Usage"
msgstr "استخدام القسم"

#: SensorManager.cpp:164
#, kde-format
msgid "Used Space"
msgstr "المساحة المُستخدَمة"

#: SensorManager.cpp:165
#, kde-format
msgid "Free Space"
msgstr "المساحة الحرّة"

#: SensorManager.cpp:166
#, kde-format
msgid "Fill Level"
msgstr "درجة الملء"

#: SensorManager.cpp:167
#, kde-format
msgid "Used Inodes"
msgstr "العُقد المُستخدَمة"

#: SensorManager.cpp:168
#, kde-format
msgid "Free Inodes"
msgstr "العُقد الحرّة"

#: SensorManager.cpp:169
#, kde-format
msgid "Inode Level"
msgstr "مستوى العُقد"

#: SensorManager.cpp:170
#, kde-format
msgid "System"
msgstr "النظام"

#: SensorManager.cpp:171
#, kde-format
msgid "Uptime"
msgstr "زمن التشغيل"

#: SensorManager.cpp:172
#, kde-format
msgid "Linux Soft Raid (md)"
msgstr ""

#: SensorManager.cpp:173
#, kde-format
msgid "Processors"
msgstr "المعالجات"

#: SensorManager.cpp:174
#, kde-format
msgid "Cores"
msgstr "الأنوية"

#: SensorManager.cpp:175
#, kde-format
msgid "Number of Blocks"
msgstr "عدد الكُتل"

#: SensorManager.cpp:176
#, kde-format
msgid "Total Number of Devices"
msgstr "العدد الإجمالي للأجهزة"

#: SensorManager.cpp:177
#, kde-format
msgid "Failed Devices"
msgstr "الأجهزة التي فشلت"

#: SensorManager.cpp:178
#, kde-format
msgid "Spare Devices"
msgstr "الأجهزة الاحتياطية"

#: SensorManager.cpp:179
#, kde-format
msgid "Number of Raid Devices"
msgstr "عدد أجهزة Raid"

#: SensorManager.cpp:180
#, kde-format
msgid "Working Devices"
msgstr "الأجهزة التي تعمل"

#: SensorManager.cpp:181
#, kde-format
msgid "Active Devices"
msgstr "الأجهزة النشطة"

#: SensorManager.cpp:182
#, kde-format
msgid "Number of Devices"
msgstr "عدد الأجهزة"

#: SensorManager.cpp:183
#, kde-format
msgid "Resyncing Percent"
msgstr "نسبة إعادة التزامن"

#: SensorManager.cpp:184
#, kde-format
msgid "Disk Information"
msgstr "معلومات القرص"

#: SensorManager.cpp:185
#, kde-format
msgid "CPU Temperature"
msgstr "حرارة المعالج"

#: SensorManager.cpp:186
#, kde-format
msgid "Motherboard Temperature"
msgstr "حرارة لوحة الأم"

#: SensorManager.cpp:187
#, kde-format
msgid "Power Supply Temperature"
msgstr "حرارة مزود الطاقة"

#: SensorManager.cpp:189
#, kde-format
msgid "Filesystem Root"
msgstr "جذر نظام الملفات"

#: SensorManager.cpp:192
#, kde-format
msgid "Extra Temperature Sensor %1"
msgstr "متحسس حرارة إضافي %1"

#: SensorManager.cpp:196
#, fuzzy, kde-format
#| msgid "Temperature %1"
msgid "PECI Temperature Sensor %1"
msgstr "الحرارة %1"

#: SensorManager.cpp:197
#, fuzzy, kde-format
#| msgid "Temperature %1"
msgid "PECI Temperature Calibration %1"
msgstr "الحرارة %1"

#: SensorManager.cpp:201
#, kde-format
msgid "CPU %1"
msgstr "المعالج %1"

#: SensorManager.cpp:202
#, kde-format
msgid "Disk %1"
msgstr "القرص %1"

#: SensorManager.cpp:206
#, kde-format
msgid "Battery %1"
msgstr "البطارية %1"

#: SensorManager.cpp:207
#, kde-format
msgid "Fan %1"
msgstr "المروحة %1"

#: SensorManager.cpp:208
#, kde-format
msgid "Temperature %1"
msgstr "الحرارة %1"

#: SensorManager.cpp:211
#, kde-format
msgid "Total"
msgstr "الإجمالي"

#: SensorManager.cpp:212
#, kde-format
msgid "Software Interrupts"
msgstr "الانقطاعات البرمجية"

#: SensorManager.cpp:213
#, kde-format
msgid "Hardware Interrupts"
msgstr "الانقطاعات العتادية"

#: SensorManager.cpp:218 SensorManager.cpp:220
#, kde-format
msgid "Int %1"
msgstr "عدد صحيح %1"

#: SensorManager.cpp:223
#, kde-format
msgid "Link Quality"
msgstr "جودة الوصلة"

#: SensorManager.cpp:224
#, kde-format
msgid "Signal Level"
msgstr "مستوى الإشارة"

#: SensorManager.cpp:225
#, kde-format
msgid "Noise Level"
msgstr "مستوى الضوضاء"

#: SensorManager.cpp:226
#, kde-format
msgid "Rx Invalid Nwid Packets"
msgstr ""

#: SensorManager.cpp:227
#, kde-format
msgid "Total Rx Invalid Nwid Packets"
msgstr ""

#: SensorManager.cpp:228
#, kde-format
msgid "Rx Invalid Crypt Packets"
msgstr ""

#: SensorManager.cpp:229
#, kde-format
msgid "Total Rx Invalid Crypt Packets"
msgstr ""

#: SensorManager.cpp:230
#, kde-format
msgid "Rx Invalid Frag Packets"
msgstr ""

#: SensorManager.cpp:231
#, kde-format
msgid "Total Rx Invalid Frag Packets"
msgstr ""

#: SensorManager.cpp:232
#, kde-format
msgid "Tx Excessive Retries Packets"
msgstr ""

#: SensorManager.cpp:233
#, kde-format
msgid "Total Tx Excessive Retries Packets"
msgstr ""

#: SensorManager.cpp:234
#, kde-format
msgid "Invalid Misc Packets"
msgstr ""

#: SensorManager.cpp:235
#, kde-format
msgid "Total Invalid Misc Packets"
msgstr ""

#: SensorManager.cpp:236
#, kde-format
msgid "Missed Beacons"
msgstr ""

#: SensorManager.cpp:237
#, kde-format
msgid "Total Missed Beacons"
msgstr ""

#: SensorManager.cpp:239
#, kde-format
msgid "Log Files"
msgstr "ملفّات السّجلّ"

#: SensorManager.cpp:243
#, kde-format
msgctxt "the unit 1 per second"
msgid "1/s"
msgstr "1/ثا"

#: SensorManager.cpp:244
#, kde-format
msgid "kBytes"
msgstr "ك.بايت"

#: SensorManager.cpp:245
#, kde-format
msgctxt "the unit minutes"
msgid "min"
msgstr "دق"

#: SensorManager.cpp:246
#, kde-format
msgctxt "the frequency unit"
msgid "MHz"
msgstr "م.هيرتز"

#: SensorManager.cpp:247
#, kde-format
msgctxt "a percentage"
msgid "%"
msgstr "%"

#: SensorManager.cpp:248
#, kde-format
msgctxt "the unit milliamperes"
msgid "mA"
msgstr "م.أمبير"

#: SensorManager.cpp:249
#, kde-format
msgctxt "the unit milliampere hours"
msgid "mAh"
msgstr "م.أمبير-ساعة"

#: SensorManager.cpp:250
#, kde-format
msgctxt "the unit milliwatts"
msgid "mW"
msgstr "م.واط"

#: SensorManager.cpp:251
#, kde-format
msgctxt "the unit milliwatt hours"
msgid "mWh"
msgstr "م.واط-ساعة"

#: SensorManager.cpp:252
#, kde-format
msgctxt "the unit millivolts"
msgid "mV"
msgstr "م.فولت"

#: SensorManager.cpp:255
#, kde-format
msgid "Integer Value"
msgstr "قيمة صحيحة"

#: SensorManager.cpp:256
#, kde-format
msgid "Floating Point Value"
msgstr "قيمة نقطة كسرية"

#: SensorManager.cpp:259
#, kde-format
msgid "Log File"
msgstr "ملفّ السّجلّ"

#: SensorShellAgent.cpp:109
#, kde-format
msgid "Could not run daemon program '%1'."
msgstr "تعذّر تشغيل العفريت '%1'."

#: SensorShellAgent.cpp:116
#, kde-format
msgid "The daemon program '%1' failed."
msgstr "فشل تشغيل العفريت '%1'."

#: SensorSocketAgent.cpp:91
#, kde-format
msgid "Connection to %1 refused"
msgstr "رُفِض الاتصال بـ %1"

#: SensorSocketAgent.cpp:94
#, kde-format
msgid "Host %1 not found"
msgstr "لم يُعثَر على المضيف %1"

#: SensorSocketAgent.cpp:97
#, kde-format
msgid ""
"An error occurred with the network (e.g. the network cable was accidentally "
"unplugged) for host %1."
msgstr "حدثت مشكلة في الشبكة (مثلًا، فُصِل كبل الشبكة) للمضيف %1."

#: SensorSocketAgent.cpp:100
#, kde-format
msgid "Error for host %1: %2"
msgstr "الأخطاء للمضيف %1: %2"
